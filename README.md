`pip install flask`

`pip install python`

If running on windows:

`python -m venv env`

`env\scripts\activate`

If on macOS/Linux

`sudo apt-get install python3-venv    # If needed
python3 -m venv env`

`source env/bin/activate`