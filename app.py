from flask import Flask, flash, redirect, render_template, request, session, abort
from datetime import datetime
import re
import os
from flask import render_template




app = Flask(__name__)


@app.route('/')
def home():
    if not session.get('logged_in'):
        return render_template('login.htm')
    else:
        return "Hello Boss!"



@app.route('/login', methods=['POST'])
def do_admin_login():
    if request.form['password'] == 'password' and request.form['username'] == 'admin':
        session['logged_in'] = True
    else:
        flash('wrong password!')
        return home()

    def fizzbuzz(n):

        if n % 3 == 0 and n % 5 == 0:
            return 'FizzBuzz'
        elif n % 3 == 0:
            return 'Fizz'
        elif n % 5 == 0:
            return 'Buzz'
        else:
            return str(n)

    

@app.route("/about")
def about():
    return render_template("about.htm")

@app.route("/contact")
def contact():
    return render_template("contact.htm")

@app.route("/hello/<name>")
def hello_there(name = None):
    return render_template(
        "happy.htm",
        name=name,
        date=datetime.now()
    )

@app.route("/api/data")
def get_data():
    return app.send_static_file("data.json")

    
    match_object = re.match("[a-zA-Z]+", name)

    if match_object:
        clean_name = match_object.group(0)
    else:
        clean_name = "Friend"

    content = "Hello there, " + clean_name + "! It's " + formatted_now
    return render_template(
        "happy.htm",
        title="Hello, user",
        content=content
    )

@app.route("/logout")
def logout():
    session['logged_in'] = False
    return home()